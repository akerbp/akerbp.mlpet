feature\_engineering module
---------------------------

.. autosummary::

   akerbp.mlpet.feature_engineering.add_formations_and_groups
   akerbp.mlpet.feature_engineering.add_gradient_features
   akerbp.mlpet.feature_engineering.add_log_features
   akerbp.mlpet.feature_engineering.add_petrophysical_features
   akerbp.mlpet.feature_engineering.add_rolling_features
   akerbp.mlpet.feature_engineering.add_sequential_features
   akerbp.mlpet.feature_engineering.calculate_AI
   akerbp.mlpet.feature_engineering.calculate_CALI_BS
   akerbp.mlpet.feature_engineering.calculate_FI
   akerbp.mlpet.feature_engineering.calculate_LFI
   akerbp.mlpet.feature_engineering.calculate_LI
   akerbp.mlpet.feature_engineering.calculate_PR
   akerbp.mlpet.feature_engineering.calculate_RAVG
   akerbp.mlpet.feature_engineering.calculate_VPVS
   akerbp.mlpet.feature_engineering.calculate_VSH
   akerbp.mlpet.feature_engineering.guess_BS_from_CALI

.. automodule:: akerbp.mlpet.feature_engineering
   :members:
   :undoc-members:
   :show-inheritance:
