utilities module
----------------

.. autosummary::

   akerbp.mlpet.utilities.df_split_train_test
   akerbp.mlpet.utilities.drop_rows_wo_label
   akerbp.mlpet.utilities.feature_target_split
   akerbp.mlpet.utilities.get_col_types
   akerbp.mlpet.utilities.get_formation_tops
   akerbp.mlpet.utilities.get_well_metadata
   akerbp.mlpet.utilities.normalize
   akerbp.mlpet.utilities.standardize_curve_names
   akerbp.mlpet.utilities.standardize_group_formation_name
   akerbp.mlpet.utilities.standardize_names
   akerbp.mlpet.utilities.train_test_split
   akerbp.mlpet.utilities.wells_split_train_test

.. automodule:: akerbp.mlpet.utilities
   :members:
   :undoc-members:
   :show-inheritance:
