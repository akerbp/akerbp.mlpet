dataset module
--------------

.. autosummary::

   akerbp.mlpet.dataset.Dataset

.. automodule:: akerbp.mlpet.dataset
   :members:
   :undoc-members:
   :show-inheritance:
