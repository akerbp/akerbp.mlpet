.. akerbp.mlpet documentation master file, created by
   sphinx-quickstart on Fri Jan 21 10:00:40 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Package Documentation

   akerbp.mlpet

.. include:: ../../README.md
   :parser: myst_parser.sphinx_




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
